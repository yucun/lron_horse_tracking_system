# -*- coding: utf-8 -*-
from datetime import datetime
from configparser import ConfigParser
import socket
import requests,json
import logging,os

# 讀取設定檔案
cfg = ConfigParser()
cfg.read(['../config/confidential.ini'])

# 基本資訊
host = cfg['socket_server']['host']
port = int(cfg['socket_server']['port'])

# log輸出
path = os.path.join('.','log')
if not os.path.exists(path):
    os.mkdir(path)
today = str(datetime.now().strftime('%Y-%m-%d'))
fileName=today+'.log'
FORMAT = '%(asctime)s %(levelname)s: %(message)s'
logging.basicConfig(level=logging.DEBUG, filename=os.path.join('.','log',fileName), filemode='a', format=FORMAT)

# 建立socket UDP Server
def socket_build():
    server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    server.bind((host, port))
    logging.info('Server start at: {}'.format(server.getsockname()))
    return server

# 取得現在時間
def now_time():
    current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    return current_time
    
# 資料解析
def data_parsing(data):
    # 接收到的資料做切割
    ID = data.decode('utf-8').split(',')[0]
    batt = data.decode('utf-8').split(',')[1]
    SQ = data.decode('utf-8').split(',')[2]
    latitude = data.decode('utf-8').split(',')[3]
    longitude = data.decode('utf-8').split(',')[4]
    
    # 電量換算與狀態處理
    batt, batt_status = batt_processing(batt)
    # 訊號強度換算處理並加上單位
    SQ = SQ_processing(SQ)
    # 經緯度換算處理
    latitude,longitude = coordinate_processing(latitude,longitude)
    return ID, batt, batt_status, SQ, latitude, longitude

# 電量換算與狀態處理
def batt_processing(batt):
    # 電量換算
    batt = round(float(batt) / 0.3, 2)
    # 電量狀態判斷
    if batt > 7.6:
        batt_status = "電量充足"
    else:
        batt_status = "電量不足"
    return batt, batt_status

# 訊號強度換算處理並加上單位
def SQ_processing(SQ):
    SQ = str(-1 * ( 113 - int(SQ) * 2 )) + 'dbm'
    return SQ

# 經緯度換算處理
def coordinate_processing(latitude,longitude):
    latitude = int(latitude[:2]) + float(latitude[2:]) / 60
    longitude = int(longitude[:3]) + float(longitude[3:]) / 60
    return latitude, longitude

# MBS資料紀錄
def MBS_information_record(ID, batt, batt_status, SQ, latitude, longitude, current_time):
    post_data = json.dumps({
        'MBS_ID' : ID, 
        'MBS_batt' : batt,
        'MBS_batt_status' : batt_status,
        'MBS_SQ' : SQ,
        'latitude' : latitude,
        'longitude' : longitude,
        'timestamp' : current_time
    })
    header = {'Content-Type': 'application/json'}
    response = requests.post(cfg['DB_proxy']['MBS_information_record'], data = post_data, headers = header)
    if response.status_code == requests.codes.ok:
        logging.info(response.text)

def main():
    # 建立socket UDP Server
    socket_server = socket_build()

    while True:
        # 等待連線
        data, address = socket_server.recvfrom(1024)
        logging.info('Server received from：{}'.format(address))
        logging.info('Data：{}'.format(data.decode('utf-8')))

        # 取得現在時間
        current_time = now_time()
        logging.info('Current Time：', current_time)

        # 資料解析
        ID, batt, batt_status, SQ, latitude, longitude = data_parsing(data)
        # MBS資料紀錄
        MBS_information_record(ID, batt, batt_status, SQ, latitude, longitude, current_time)


if __name__ == '__main__':
    main()
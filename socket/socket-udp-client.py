# -*- coding: utf-8 -*-
import socket

# 基本資訊
host = '127.0.0.1'
port = 7000


def main():
    server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    address = (host, port)

    while True:
        msg = '01,2.4,20,2245.3534,12020.0982'
        msg = msg.encode()
        server.sendto(msg, address)
        print("接收到服務端的消息:", server.recv(1024))

if __name__ == '__main__':
    main()
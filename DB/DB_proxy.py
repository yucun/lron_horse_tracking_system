# -*- coding: utf-8 -*-
from configparser import ConfigParser
from flask_cors import CORS
from flask import Flask, request
from datetime import datetime
import logging,os
import pymysql
import json

app = Flask(__name__)
CORS(app, supports_credentials=True)

# 讀取設定檔案
cfg = ConfigParser()
cfg.read(['../config/confidential.ini'])

# log輸出
path = os.path.join('.','log')
if not os.path.exists(path):
    os.mkdir(path)
today = str(datetime.now().strftime('%Y-%m-%d'))
fileName=today+'.log'
FORMAT = '%(asctime)s %(levelname)s: %(message)s'
logging.basicConfig(level=logging.DEBUG, filename=os.path.join('.','log',fileName), filemode='a', format=FORMAT)

# DB連線
def connect_DB():
    logging.info({'msg':'資料庫連線中...'})
    conn = pymysql.connect(
        host = cfg['DB']['host'],
        user = cfg['DB']['user'],
        password = cfg['DB']['password'],
        port = int(cfg['DB']['port']),
        db = cfg['DB']['database'],
        charset = cfg['DB']['charset']
    )
    logging.info({'msg':'資料庫已連線'})
    return conn

# 寫入DB
def write_db(command, value):
    conn = connect_DB()# DB連線
    cursor = conn.cursor()# 獲取操作遊標
    try:
        cursor.execute(command, value)
    except Exception as error:
        logging.error({'msg':error})
    else:
        conn.commit()
    conn.close()# 關閉連線
    logging.info({'msg':'資料庫已關閉連線'})

# 查詢資料-單純語法
def search_db(command):
    conn = connect_DB()# DB連線
    cursor = conn.cursor()# 獲取操作遊標
    try:
        cursor.execute(command)
    except Exception as error:
        logging.error({'msg':error})
    conn.close()# 關閉連線
    logging.info({'msg':'資料庫已關閉連線'})
    return cursor

# 取得現在時間
def now_time():
    current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    return current_time

# 狀態判斷，回應對應語法與值
def status_if(uuid, MBS_ID, current_time, status):
    if status == 'start':
        value = (uuid, MBS_ID, current_time)
        command = "INSERT INTO historical_time(ID, MBS_ID, start_time) VALUES (%s, %s, %s)"
    else:
        value = (current_time, uuid)
        command = "UPDATE historical_time SET end_time = %s WHERE ID = %s"
    return value, command

# 動態生成list作為回傳內容-時間選單
def dynamic_list_time(result):
    list_result = []
    result_count = len(result)
    for i in range(result_count):
        list_result.append(result[i][0])
    return list_result

# 動態生成list作為回傳內容-座標列表
def dynamic_list_coordinate(result):
    list_result = []
    result_count = len(result)
    for i in range(result_count):
        list_result.append([])
        list_result[i].append(result[i][0])
        list_result[i].append(result[i][1])
    return list_result

# 尋找特定時間範圍內所有座標，並生成list
def specific_time(start_time, end_time):
    command = "SELECT latitude,longitude FROM MBS_information WHERE timestamp BETWEEN '"+ start_time +"' AND '"+ end_time +"'"
    cursor = search_db(command)
    result = cursor.fetchall()# 獲取查詢結果
    list_result = dynamic_list_coordinate(result)# 動態生成list作為回傳內容-座標列表
    return list_result

# 即時更新判斷
def realtime_update_if(uuid, status):
    # 查詢開始與結束時間
    command = "SELECT start_time,end_time FROM historical_time WHERE ID='"+ uuid +"'"
    cursor = search_db(command)
    result = cursor.fetchall()# 獲取查詢結果
    start_time = result[0][0]
    # 狀態判斷，以用來取決結束時間取值
    if status == 'start': 
        end_time = now_time()# 取得現在時間，當作查詢結束時間
    else:
        end_time = result[0][1]
    # 尋找特定時間範圍內所有座標，並生成list
    list_result = specific_time(start_time,end_time)
    return list_result

# 紀錄MBS資訊
@app.route('/MBS_information_record', methods=['POST'])
def MBS_information_record():
    command = "INSERT INTO MBS_information(MBS_ID, MBS_batt, MBS_batt_status, MBS_SQ, latitude, longitude, timestamp) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    MBS_ID = request.json['MBS_ID']
    MBS_batt = request.json['MBS_batt']
    MBS_batt_status = request.json['MBS_batt_status']
    MBS_SQ = request.json['MBS_SQ']
    latitude = request.json['latitude']
    longitude = request.json['longitude']
    timestamp = request.json['timestamp']
    value = (MBS_ID, MBS_batt, MBS_batt_status, MBS_SQ, latitude, longitude, timestamp)  
    # 寫入DB
    write_db(command, value)
    return {'Status' : 'Ok'}

# 目前位置查詢=最後一筆座標查詢
@app.route('/current_coordinates', methods=['POST'])
def current_coordinates():
    respond = request.get_json(force = True)
    MBS_ID = str(respond['MBS_ID'])
    command = "SELECT latitude,longitude FROM MBS_information WHERE MBS_ID="+ MBS_ID +" ORDER BY ID DESC"
    cursor = search_db(command)
    result = cursor.fetchone()# 取得第一筆資料
    return {'latitude' : result[0], 'longitude' :  result[1]}

# 抓取最新MBS資訊
@app.route('/MBS_information', methods=['POST'])
def MBS_information():
    respond = request.get_json(force = True)
    MBS_ID = str(respond['MBS_ID'])
    command = "SELECT MBS_ID,MBS_batt,MBS_batt_status,MBS_SQ,timestamp FROM MBS_information WHERE MBS_ID="+ MBS_ID +" ORDER BY ID DESC"
    cursor = search_db(command)
    result = cursor.fetchone()# 取得第一筆資料
    return {'MBS_ID' : result[0], 'MBS_batt' :  result[1], 'MBS_batt_status' :  result[2], 'MBS_SQ' :  result[3], 'timestamp' :  result[4]}

# 時間紀錄
@app.route('/time_record', methods=['POST'])
def time_record():
    respond = request.get_json(force = True)
    uuid = str(respond['uuid'])
    MBS_ID = str(respond['MBS_ID'])
    status = str(respond['status'])
    current_time = now_time()# 取得現在時間
    # 狀態判斷，回應對應語法與值
    value, command = status_if(uuid, MBS_ID, current_time, status)
    # 寫入DB
    write_db(command, value)
    return {'Status' : 'Ok'}

# 取得所有開始時間，以製作時間選單
@app.route('/time_menu', methods=['POST'])
def time_menu():
    respond = request.get_json(force = True)
    MBS_ID = str(respond['MBS_ID'])
    command = "SELECT start_time FROM historical_time WHERE MBS_ID="+ MBS_ID +" ORDER BY start_time DESC"
    cursor = search_db(command)
    result = cursor.fetchall()# 獲取查詢結果
    list_result = dynamic_list_time(result)# 動態生成list作為回傳內容-時間選單
    return {'time_menu':list_result}

# 找尋時間範圍內所有座標-歷史紀錄
@app.route('/specific_coordinates', methods=['POST'])
def specific_coordinates():
    respond = request.get_json(force = True)
    MBS_ID = str(respond['MBS_ID'])
    select_time = str(respond['select_time'])
    # 查詢結束時間
    command = "SELECT end_time FROM historical_time WHERE MBS_ID="+ MBS_ID +" AND start_time='"+ select_time +"'"
    cursor = search_db(command)
    result = cursor.fetchall()# 獲取查詢結果
    end_time = result[0][0]
    # 尋找特定時間範圍內所有座標，並生成list
    list_result = specific_time(select_time,end_time)
    return {'coordinate_menu':list_result}

# 找尋時間範圍內所有座標-即時更新
@app.route('/realtime_coordinate_update', methods=['POST'])
def realtime_coordinate_update():
    respond = request.get_json(force = True)
    uuid = str(respond['uuid'])
    status = str(respond['status'])
    list_result = realtime_update_if(uuid, status)
    return {'coordinate_menu':list_result}

# 抓取所有MBS_ID
@app.route('/all_MBS_ID', methods=['GET'])
def all_MBS_ID():
    command = "SELECT DISTINCT MBS_ID FROM MBS_information ORDER BY MBS_ID"
    cursor = search_db(command)
    result = cursor.fetchall()# 獲取查詢結果
    list_result = dynamic_list_time(result)# 動態生成list作為回傳內容-時間選單
    return {'MBS_ID_menu':list_result}

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=False, port=6681)
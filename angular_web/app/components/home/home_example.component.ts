import { Component, OnInit, ViewChild  } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { } from '@types/googlemaps';

declare var $: any;

@Component({
    selector: 'my-home',
    templateUrl: 'components/home/home.component.html',
    styleUrls: ['components/home/home.component.css']
})
export class HomeComponent {
    name: string = "Home page";
    users: {};

    constructor(http: Http) {
        http.get("/users")
            .map(data => data.json())
            .subscribe((data) => this.users = data);
    }

    // 地圖
    @ViewChild('googleMap') gmapElement: any;
    map: google.maps.Map;
    
    ngOnInit(){
        // MBS 基本資料
        let MBS_ID = '01';
        // 初始
        let times_run = 0;
        // API URL
        let URL_prefix = 'http://ava:6681/'
        let MBS_information_URL = URL_prefix + 'MBS_information';
        let current_coordinates_URL = URL_prefix + 'current_coordinates';
        let time_record_URL = URL_prefix + 'time_record';
        let time_menu_URL = URL_prefix + 'time_menu';
        let specific_coordinates_URL = URL_prefix + 'specific_coordinates';
        let realtime_coordinate_update_URL = URL_prefix + 'realtime_coordinate_update';
        let all_MBS_ID_URL = URL_prefix + 'all_MBS_ID';

        // MBS資訊更新
        function MBS_information_update(){
            $.ajax({
                url: MBS_information_URL,
                contentType: "application/json", 
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify({"MBS_ID":MBS_ID}),
                cache: false,
                timeout: 1000,
                success: function (msg) {
                    $('.value_id').html(msg['MBS_ID']);
                    $('.value_batt').html(msg['MBS_batt']);
                    $('.value_status').html(msg['MBS_batt_status']);
                    $('.value_SQ').html(msg['MBS_SQ']);
                    $('.value_updateTime').html(msg['timestamp']);
                },
                error: function () {
                    console.log('MBS_information_update ajax error!');
                }
            })
        }

        // 抓取最新經緯度
        function latest_location(){
            $.ajax({
                url: current_coordinates_URL,
                contentType: "application/json", 
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify({"MBS_ID":MBS_ID}),
                cache: false,
                timeout: 1000,
                success: function (coordinate) {
                    $('#now_latitude').html(coordinate['latitude']);
                    $('#now_longitude').html(coordinate['longitude']);
                },
                error: function () {
                    console.log('latest_location ajax error!');
                }
            })
        }

        // 更新路線地圖處理
        function update_route_map_processing(msg){
            let coordinate_menu = msg['coordinate_menu'];
            let coordinate_length = coordinate_menu.length;
            let markers = [];
            for(let i = 0 ; i < coordinate_length ; i++){
                markers.push({ lat : coordinate_menu[i][0] ,lng : coordinate_menu[i][1] });
            }
            map_update(markers);
        }

        // 抓取該時間點所有座標-歷史紀錄
        function specific_coordinates(){
            let select_time = $('#time_selection').find('option:selected').text();
            $.ajax({
                url: specific_coordinates_URL,
                contentType: "application/json", 
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify({"MBS_ID":MBS_ID,"select_time":select_time}),
                cache: false,
                timeout: 1000,
                success: function (msg) {
                    // 更新路線地圖處理
                    update_route_map_processing(msg);
                },
                error: function () {
                    console.log('specific_coordinates ajax error!');
                }
            })
        }

        // 即時更新路線地圖
        function realtime_coordinate_update(status, uuid){
            $.ajax({
                url: realtime_coordinate_update_URL,
                contentType: "application/json", 
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify({"uuid":uuid,"status":status}),
                cache: false,
                timeout: 1000,
                success: function (msg) {
                    // 更新路線地圖處理
                    update_route_map_processing(msg);
                },
                error: function () {
                    console.log('realtime_coordinate_update ajax error!');
                }
            })
        }
        // 地圖標註與路徑繪製
        function map_update(markers){
            let latitude = Number($('#now_latitude').html());
            let longitude = Number($('#now_longitude').html());
            let HTMLElement = document.getElementById('now_map') as HTMLElement;
            // 如果沒有座標傳進來，顯示目前位置
            if(!markers){
                let new_markers = [
                    { lat: latitude,lng: longitude }
                ]
                markers = new_markers;
            }
            // 地圖設定
            let mapProp = {
                center: new google.maps.LatLng(latitude, longitude),
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
                // mapTypeId: google.maps.MapTypeId.HYBRID
                // mapTypeId: google.maps.MapTypeId.SATELLITE
                // mapTypeId: google.maps.MapTypeId.TERRAIN
            };  
            let map = new google.maps.Map(HTMLElement, mapProp);
            // 畫線
            const flightPath = new google.maps.Polyline({
                path: markers,
                geodesic: true,
                strokeColor: "#FF0000",
                strokeOpacity: 1.0,
                strokeWeight: 2,
            });
            flightPath.setMap(map);
            // 標示座標點
            let index = 0;
            markers.forEach(function (marker) {
                (function (marker) {
                    let mark = new google.maps.Marker({
                        position: new google.maps.LatLng(marker.lat, marker.lng),
                        icon: {
                            url: './images/coordinate.png',
                            scaledSize: new google.maps.Size(30, 30)
                        }
                    });
                    mark.setMap(map);
                    index++;
                })(marker)
            })
        }

        // 初始化網頁資訊
        function init_information(){
            times_run++;
            if(times_run == 1){
                MBS_information_update();
                latest_location();
            }else if(times_run == 2){
                map_update('');
            }
        }

        // 更新判斷
        function counting(update_if){
            if(update_if == 'init'){// 初始化網頁資訊
                setInterval(init_information, 1000);
            }else if('map_update'){// 每5秒抓取最新座標，並於每10秒更新地圖
                setInterval(latest_location, 5000);
                setInterval(map_update(''), 10000);
            }
        }

        // 時間紀錄
        function record_time(status, uuid){
            $.ajax({
                url: time_record_URL,
                contentType: "application/json", 
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify({"uuid":uuid, "MBS_ID":MBS_ID, "status":status}),
                cache: false,
                timeout: 1000,
                success: function (msg) {
                    // 即時更新地圖
                    if(status=='start'){
                        let temp_num = 0;
                        let interval = setInterval(function(){
                            temp_num++;
                            if((temp_num % 5) == 0){// 每五秒更新
                                realtime_coordinate_update('start', uuid);
                            }
                            $('#end_btn').click(function(){
                                $('#btn_status').html('end');
                            })
                            if($('#btn_status').html() == 'end'){
                                clearTimeout(interval);
                            }
                        },1000);
                    time_dynamic_menu();// 時間選單更新
                    }else{
                        realtime_coordinate_update('end', uuid);
                    }
                    console.log('Update map success');
                },
                error: function () {
                    console.log('record_time ajax errorq!');
                }
            })
        }

        // UUIDv4生成
        function uuidv4() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }

        // 狀態判斷以用來決定是否生成新的uuid
        let temp_number = '';
        function status_if(status){
            if(status == 'start'){
                let uuid = uuidv4();
                temp_number = uuid;
            }
            return temp_number
        }

        // 動態生成時間選單
        function time_dynamic_menu(){
            $.ajax({
                url: time_menu_URL,
                contentType: "application/json", 
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify({"MBS_ID":MBS_ID}),
                cache: false,
                timeout: 1000,
                success: function (msg) {
                    let time_menu_length = (msg['time_menu']).length;
                    let select = document.getElementById("time_selection");
                    // 刪除舊有的
                    select.options.length = 1;
                    // 新增新的
                    for(let i = 0 ; i < time_menu_length ; i++){ 
                        let option = document.createElement("option"); 
                        option.appendChild(document.createTextNode(msg['time_menu'][i])); 
                        select.appendChild(option);
                    }
                },
                error: function () {
                    console.log('time_dynamic_menu ajax error!');
                }
            })
        }

        // MBS_ID選單更新
        function MBS_ID_menu_update(){
            $.ajax({
                url: all_MBS_ID_URL,
                contentType: "application/json", 
                type: 'GET',
                dataType: 'json',
                cache: false,
                timeout: 1000,
                success: function (msg) {
                    let MBS_ID_menu_length = (msg['MBS_ID_menu']).length;
                    let select = document.getElementById("MBS_ID_selection");
                    // 刪除舊有的
                    select.options.length = 0;
                    // 新增新的
                    for(let i = 0 ; i < MBS_ID_menu_length ; i++){ 
                        let option = document.createElement("option"); 
                        option.appendChild(document.createTextNode(msg['MBS_ID_menu'][i])); 
                        select.appendChild(option);
                    }
                },
                error: function () {
                    console.log('MBS_information_update ajax error!');
                }
            })
        }

        // 更新為新的MBS_ID資訊
        function MBS_ID_selection(){
            let MBS_ID_selection = $('#MBS_ID_selection').find('option:selected').text();
            MBS_ID = MBS_ID_selection;
            $('#start_btn').css('pointer-events','auto');// 開啟開始記錄按鈕，直到按下結束
            $('#end_btn').css('pointer-events','none');// 關閉結束記錄按鈕，直到按下開始
            $('#end_btn').css('background-color','#afafafe1');// 結束紀錄按鈕顏色更改
            times_run = 0;
            init_information();
            time_dynamic_menu();// 時間選單更新
        }

        $(document).ready(function(){
            // 初始化網頁資訊
            counting('init');
            $('#start_btn').css('pointer-events','auto');// 開啟開始記錄按鈕，直到按下結束
            $('#end_btn').css('pointer-events','none');// 關閉結束記錄按鈕，直到按下開始
            $('#end_btn').css('background-color','#afafafe1');// 結束紀錄按鈕顏色更改
            time_dynamic_menu();// 時間選單更新
            MBS_ID_menu_update();// MBS_ID選單更新

            // 每10秒更新一次MBS資訊
            setInterval(MBS_information_update , 10000);

            // 地圖定時更新
            counting('map_update');

            // 地圖顯示（目前位置）
            $('.current_position').click(function(){
                map_update('');
            });

            // 開始與結束紀錄按鈕相關操作
            // 開始記錄按鈕被點選，記錄時間至DB、更改按鈕css、即時更新地圖
            $('#start_btn').click(function(){
                let uuid = status_if('start');// 狀態判斷以用來決定是否生成新的uuid
                record_time('start',uuid);
                $('#start_btn').css('pointer-events','none');// 關閉開始記錄按鈕，直到按下結束
                $('#end_btn').css('pointer-events','auto');// 開啟結束記錄按鈕，直到按下開始
                $('#start_btn').css('background-color','#afafafe1');// 開始紀錄按鈕顏色更改
                $('#end_btn').css('background-color','#d63f3985');// 結束紀錄按鈕顏色更改
                $('.current_position').css('pointer-events','none');// 關閉目前所在位置按鈕，直到按下結束
                $('.current_position').css('background-color','#afafafe1');// 目前所在位置按鈕顏色更改
                $('.search_btn').css('pointer-events','none');// 關閉查詢按鈕，直到按下結束
                $('.search_btn').css('background-color','#afafafe1');// 查詢按鈕顏色更改
            });
            // 結束記錄按鈕被點選，記錄時間至DB、更改按鈕css、即時更新地圖
            $('#end_btn').click(function(){
                let uuid = status_if('end');// 狀態判斷以用來決定是否生成新的uuid
                record_time('end',uuid);
                $('#start_btn').css('pointer-events','auto');// 開啟開始記錄按鈕，直到按下結束
                $('#end_btn').css('pointer-events','none');// 關閉結束記錄按鈕，直到按下開始
                $('#start_btn').css('background-color','#2eb951c0');// 開始紀錄按鈕顏色更改
                $('#end_btn').css('background-color','#afafafe1');// 結束紀錄按鈕顏色更改
                $('.current_position').css('pointer-events','auto');// 開啟目前所在位置按鈕，直到按下開始
                $('.current_position').css('background-color','#E1B121');// 目前所在位置按鈕顏色更改
                $('.search_btn').css('pointer-events','auto');// 開啟查詢按鈕，直到按下開始
                $('.search_btn').css('background-color','#3b9ada');// 查詢按鈕顏色更改
            });

            // 按下查詢，便會查詢該時段路線
            $('#route_search_btn').click(function(){
                if($('#time_selection').find('option:selected').text() == '請選擇查詢時間'){
                    alert('請選擇預查詢時間');
                }else{
                    specific_coordinates();// 抓取該時間點所有座標-歷史紀錄
                }
            })

            // 按下查詢，便會查詢對應MBS_ID資料
            $('#MBS_search_btn').click(function(){
                MBS_ID_selection();
            })
        })
    }
}

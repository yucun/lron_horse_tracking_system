import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `
<router-outlet></router-outlet>
<p id='menu'><a [routerLink]="['/']">Home</a> | <a [routerLink]="['/about/', { id: 2 }]">About</a></p>`,
})
export class AppComponent {
    name: string = "Angular 2 on Express";

    constructor() {}
}

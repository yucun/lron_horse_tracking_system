# 說明
> 2021年資訊管理導論期末成果，為一個愛心鐵馬追蹤系統，用以幫助管理員清楚知道愛心鐵馬之定位，縮短其管理時間。
> 由Socket Server接收MBS訊息，並記錄至DB。

# 事前準備
* 安裝Node.js+npm
* 安裝Angular CLI
* 安裝Python

# 使用技術與對應版本
* Socket Server
* Node.js v14.17.0 + npm v6.14.13 + express 
* Angular v12.0.3(但實際使用2.1.1)
* Python v3.8.2
* MySQL

# 啟動方法
* angular_web
	*  npm install（只需要執行第一次）
	*  npm start
* DB_proxy
	* python DB_proxy.py     
* Socket Server
	* python socket-udp-server.py

# 注意事項
* 進入目錄後需執行
	* 步驟ㄧ
		* cp ./config/confidential_example.ini ./config/confidential.ini
		* 打開confidential.ini，再來是將DB相關設定改為符合自身DB設定。
		* 將ava修改為對應您的host
	* 步驟二
		* cp ./angular_web/app/components/home/home_example.component.ts ./angular_web/app/components/home/home.component.ts
		* 打開home.component.ts，將ava修改為對應您的host
* Google地圖API  KEY須自行申請並更換，步驟如下：
	* 至Google地圖平台申請API KEY
	* 執行cp ./angular_web/public/index.dev_example.html ./angular_web/public/index.dev.html 
	* 將index.dev.html檔案內的YOUR_API_KEY修改為您申請的KEY。


# ＤＢ創建
```
CREATE DATABASE lron_horse_tracking_system DEFAULT CHARACTER SET utf8mb4;
```
```
CREATE TABLE `lron_horse_tracking_system`.`MBS_information` (
  ID INT NOT NULL AUTO_INCREMENT,
  MBS_ID VARCHAR(2) NOT NULL,
  MBS_batt FLOAT NOT NULL,
  MBS_batt_status VARCHAR(4) NOT NULL,
  MBS_SQ VARCHAR(7) NOT NULL,
  timestamp VARCHAR(19) NOT NULL,
  latitude FLOAT NOT NULL,
  longitude FLOAT NOT NULL,
  PRIMARY KEY (`ID`));
```
```
CREATE TABLE `lron_horse_tracking_system`.`historical_time` (
  ID VARCHAR(36) NOT NULL,
  MBS_ID VARCHAR(2) NOT NULL,
  start_time VARCHAR(30) NOT NULL,
  end_time VARCHAR(30) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`));
```

# 問題處理
[python OSError python3.8 TCP sever](https://ithelp.ithome.com.tw/questions/10199271?sc=pt)   
[如何修復 Uncaught InvalidValueError: setPosition: not a LatLng or LatLngLiteral: in property lat: not a number？](https://stackoverflow.com/questions/20585055/how-to-fix-uncaught-invalidvalueerror-setposition-not-a-latlng-or-latlnglitera/20585117)

# 參考網站
## Socket
[Day-12 Python_Socket小實作](https://ithelp.ithome.com.tw/articles/10205819)   
[Python Socket 编程--基于UDP的套接字](https://blog.csdn.net/HHG20171226/article/details/93590850)  

## angular、Node.js、express
[angular常見路由](https://angular.tw/guide/router)   
[angular參考Code](https://github.com/vladotesanovic/angular2-express-slim)   
[Node.js + Express + Angular Stack](https://medium.com/swlh/node-js-express-angular-stack-d6e328cffe6b)  
[Angular 12 + Node.js Express + MySQL example: CRUD Application](https://bezkoder.com/angular-12-node-js-express-mysql/)  
[How To Use jQuery With Angular (When You Absolutely Have To)](https://blog.bitsrc.io/how-to-use-jquery-with-angular-when-you-absolutely-have-to-42c8b6a37ff9) 
### 地圖
[開始使用 Google 地圖平台](https://developers.google.com/maps/gmp-get-started?hl=zh-tw#create-billing-account)    
[Integrate google API/MAP in your angular 2/4/5 app
](https://medium.com/@svsh227/integrate-google-api-map-in-your-angular-2-4-5-app-472bf08fdac)   
[Google地圖平台](https://developers.google.com/maps?hl=zh-tw)   
[Lat/Lng Object Literal](https://developers.google.com/maps/documentation/javascript/examples/map-latlng-literal#maps_map_latlng_literal-typescript)  
[Google Maps API - 顯示多個標記工具提示](https://stackoverflow.com/questions/6914505/google-maps-api-show-multiple-marker-tool-tips/49996508#49996508)   
[Google Maps API 學習筆記 – 2：在地圖上畫個日本結界](https://www.letswrite.tw/google-map-api-geometry/)  
[Maps JavaScript API](https://developers.google.com/maps/documentation/javascript/shapes?hl=zh-tw#draggable)    
[Google Maps API - 設計地圖標記圖案](https://www.oxxostudio.tw/articles/201801/google-maps-6-marker-image.html)    

## jQuery、js、css
[CSS 兩欄式排版](https://ithelp.ithome.com.tw/articles/10160237) 
[纯HTML+CSS实战之制作相框效果](https://blog.csdn.net/qq_41886761/article/details/86572714)
 
## Python
 [使用AJAX將JSON數據傳遞給Flask服務器？](http://hk.uwenku.com/question/p-hupebrwk-bgs.html)   
 
## MySQL
[[Python實戰應用]掌握Python連結MySQL資料庫的重要操作](https://www.learncodewithmike.com/2020/02/python-mysql.html)   
[python寫入MySQL資料庫](https://www.itread01.com/content/1543738443.html)  


